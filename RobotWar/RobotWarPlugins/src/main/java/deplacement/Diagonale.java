package deplacement;

import java.util.ArrayList;

import annotations.PluginAnnotation;
import partiePrincipale.Cadrillage;
import partiePrincipale.Case;
import partiePrincipale.IA;
import partiePrincipale.Joueur;
import plugins.DeplacementPlugin;
@PluginAnnotation(type = "DeplacementPlugin")
public class Diagonale implements DeplacementPlugin {
	static final int COUT = 4;
	static final int IA_MIN_LEVEL = 0;
	private boolean actif = true;

	public boolean actif() {
		return this.actif;
	}

	public void setActif(boolean value) {
		this.actif = value;
	}

	public String getLibelle() {
		return "Diagonale";
	}

	public int coutDeplacement() {
		return COUT;
	}

	public int iaLevelMin() {
		return IA_MIN_LEVEL;
	}

	public ArrayList<Case> casesAccessibles(Cadrillage cadrillage,
			Joueur joueur) {
		Case current = joueur.getRobot().getCaseCourante();
		Case other;
		int x = current.getX();
		int y = current.getY();
		ArrayList<Case> accessibles = new ArrayList<Case>();

		other = cadrillage.getCaseSiExistante(x + 1, y + 1);
		if (other != null && other.estLibre())
			accessibles.add(other);
		other = cadrillage.getCaseSiExistante(x - 1, y - 1);
		if (other != null && other.estLibre())
			accessibles.add(other);
		other = cadrillage.getCaseSiExistante(x + 1, y - 1);
		if (other != null && other.estLibre())
			accessibles.add(other);
		other = cadrillage.getCaseSiExistante(x - 1, y + 1);
		if (other != null && other.estLibre())
			accessibles.add(other);

		return accessibles;
	}

	public boolean estPossible(Joueur joueur, boolean checkEnergie) {
		if ((joueur.getIa().getNiveau() < IA_MIN_LEVEL)
				&& ((checkEnergie && (joueur.getRobot().getEnergie() <= COUT))))
			return false;
		else
			return true;
	}

}

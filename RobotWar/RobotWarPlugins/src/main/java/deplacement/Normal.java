package deplacement;

import java.util.ArrayList;

import partiePrincipale.Cadrillage;
import partiePrincipale.Case;
import partiePrincipale.IA;
import partiePrincipale.Joueur;
import plugins.DeplacementPlugin;
import annotations.PluginAnnotation;

@PluginAnnotation(type = "DeplacementPlugin")
public class Normal implements DeplacementPlugin {
	static final int COUT = 2;
	static final int IA_MIN_LEVEL = 0;
	private boolean actif = true;

	public boolean actif() {
		return this.actif;
	}

	public void setActif(boolean value) {
		this.actif = value;
	}

	public String getLibelle() {
		return "Normal";
	}

	public int coutDeplacement() {
		return COUT;
	}

	public int iaLevelMin() {
		return IA_MIN_LEVEL;
	}

	public ArrayList<Case> casesAccessibles(Cadrillage cadrillage, Joueur joueur) {
		Case current = joueur.getRobot().getCaseCourante();
		Case autre;
		ArrayList<Case> accessibles = new ArrayList<Case>();

		autre = cadrillage.getCase(current, 0);
		if (autre != null && autre.estLibre()) {
			accessibles.add(autre);
		}
		autre = cadrillage.getCase(current, 1);
		if (autre != null && autre.estLibre()) {
			accessibles.add(autre);
		}
		autre = cadrillage.getCase(current, 2);
		if (autre != null && autre.estLibre()) {
			accessibles.add(autre);
		}
		autre = cadrillage.getCase(current, 3);
		if (autre != null && autre.estLibre()) {
			accessibles.add(autre);
		}
		return accessibles;
	}

	public boolean estPossible(Joueur joueur, boolean checkEnergie) {
		if ((joueur.getIa().getNiveau() < IA_MIN_LEVEL)
				&& ((checkEnergie && (joueur.getRobot().getEnergie() <= COUT)))) {
			return false;
		} else {
			return true;
		}
	}
}

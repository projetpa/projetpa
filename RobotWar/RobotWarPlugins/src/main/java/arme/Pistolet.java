package arme;

import java.awt.Graphics;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import annotations.PluginAnnotation;
import partiePrincipale.Arme;
import partiePrincipale.Cadrillage;
import partiePrincipale.Case;
import partiePrincipale.Robot;
import plugins.ArmeGraphiquePlugin;

@PluginAnnotation(type = "ArmeGraphique")
public class Pistolet implements ArmeGraphiquePlugin {
	private final static int ARME_ID = 0;
	private final static int COEF_LARGEUR_PISTOLET = 2;
	private boolean actif = true;

	public boolean actif() {
		return this.actif;
	}

	public void setActif(boolean value) {
		this.actif = value;
	}

	public void display(Cadrillage cadrillage, Robot robot, Graphics g) {
		boolean possedeArme = false;
		if (robot.getCible() == null)
			return;

		g.setColor(robot.getCouleur());

		for (Arme arme : robot.getArmes()) {
			if (arme.getId() == ARME_ID)
				possedeArme = true;
		}

		if (!possedeArme)
			return;

		int largCase = robot.getCaseCourante().getLarg();

		int xSource = robot.getCaseCourante().getX();
		int xCible = robot.getCible().getCaseCourante().getX();
		int ySource = robot.getCaseCourante().getY();
		int yCible = robot.getCible().getCaseCourante().getY();

		xSource *= largCase;
		xSource += (largCase / 2);
		xCible *= largCase;
		xCible += (largCase / 2);
		ySource *= largCase;
		ySource += (largCase / 2);
		yCible *= largCase;
		yCible += (largCase / 2);

		g.drawLine(xSource, ySource, xCible, yCible);

		robot.setCible(null);
	}

	public void drawAt(Case current, int direction, Graphics g) {
		int x = current.getX();
		int y = current.getY();
		int largCase = current.getLarg();
		int largPic = largCase / COEF_LARGEUR_PISTOLET;
		int[] xPoints = new int[3];
		int[] yPoints = new int[3];

		switch (direction) {
		case 0:
			xPoints[0] = x * largCase + largPic;
			xPoints[1] = (x + 1) * largCase - largPic;
			xPoints[2] = x * largCase + (largCase / 2);

			yPoints[0] = (y + 1) * largCase;
			yPoints[1] = (y + 1) * largCase;
			yPoints[2] = (y + 1) * largCase + largPic;
			break;
		case 1:
			xPoints[0] = (x + 1) * largCase;
			xPoints[1] = (x + 1) * largCase;
			xPoints[2] = (x + 1) * largCase + largPic;

			yPoints[0] = (y + 1) * largCase - largPic;
			yPoints[1] = y * largCase + largPic;
			yPoints[2] = y * largCase + (largCase / 2);
			break;
		case 2:
			xPoints[0] = x * largCase + largPic;
			xPoints[1] = (x + 1) * largCase - largPic;
			xPoints[2] = x * largCase + (largCase / 2);

			yPoints[0] = y * largCase;
			yPoints[1] = y * largCase;
			yPoints[2] = y * largCase - largPic;
			break;
		case 3:
			xPoints[0] = x * largCase;
			xPoints[1] = x * largCase;
			xPoints[2] = x * largCase - largPic;

			yPoints[0] = y * largCase + largPic;
			yPoints[1] = (y + 1) * largCase - largPic;
			yPoints[2] = y * largCase + (largCase / 2);
			break;
		}

		g.fillPolygon(xPoints, yPoints, 3);
	}

	public String getLibelle() {
		return "Pistolet";
	}

	public static void save(Pistolet monObjet) {
		try {
			ObjectOutputStream oo = new ObjectOutputStream(
					new FileOutputStream(new File("save.dat")));
			oo.writeObject(monObjet);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

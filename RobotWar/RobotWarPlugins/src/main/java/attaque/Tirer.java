package attaque;

import annotations.PluginAnnotation;
import partiePrincipale.Arme;
import partiePrincipale.Case;
import partiePrincipale.Robot;
@PluginAnnotation(type = "Attaque")
public class Tirer implements plugins.AttaquePlugin {
	private static final int VALEUR_ATTAQUE = 40;
	private static final int PORTEE_ATTAQUE = 10;
	private static final int COUT = 20;
	private static final int ARME_ID = 0;
	private boolean actif = true;

	public boolean actif() {
		return this.actif;
	}

	public void setActif(boolean value) {
		this.actif = value;
	}

	public String getLibelle() {
		return "Tirer";
	}

	public Robot peutAttaquer(Robot robot) {
		if (robot.getEnergie() <= COUT || !robot.possedeArme(ARME_ID))
			return null;

		for (Robot autreRobot : robot.getEnnemis()) {
			Case maCase = robot.getCaseCourante();
			Case saCase = autreRobot.getCaseCourante();

			if (maCase.getDistance(saCase) <= PORTEE_ATTAQUE)
				return autreRobot;
		}
		return null;

	}

	public int valeurAttaque() {
		return VALEUR_ATTAQUE;
	}

	public int getCout() {
		return COUT;
	}

	public void attaque(Robot source, Robot cible) {
		if ((source.getCaseCourante().getDistance(cible.getCaseCourante()) <= PORTEE_ATTAQUE)
				&& (source.getEnergie() > COUT)) {
			cible.perteEnergie(VALEUR_ATTAQUE);
			source.perteEnergie(COUT);
			source.setCible(cible);
		}

	}

}

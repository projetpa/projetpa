package robot;

import java.awt.Graphics;

import annotations.PluginAnnotation;
import partiePrincipale.Robot;
import plugins.RobotGraphiquePlugin;

@PluginAnnotation(type = "RobotGraphique")
public class FormeGeometrique implements RobotGraphiquePlugin {
	private static final int version = 0;
	private boolean actif = true;

	public boolean actif() {
		return this.actif;
	}

	public void setActif(boolean value) {
		this.actif = value;
	}

	public void display(Robot robot, Graphics g) {
		int x = robot.getCaseCourante().getX();
		int y = robot.getCaseCourante().getY();
		int larg = robot.getCaseCourante().getLarg();
		g.setColor(robot.getCouleur());
		g.fillRect(x * larg, y * larg, larg, larg);
	}

	public String getLibelle() {
		return "Forme Geometrique";
	}

	public int getVersion() {
		return version;
	}

}

package robot;

import java.awt.AlphaComposite;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.MalformedURLException;
import java.nio.file.Paths;

import javax.swing.ImageIcon;

import annotations.PluginAnnotation;
import partiePrincipale.Robot;
@PluginAnnotation(type = "RobotGraphique")
public class AvecImage implements plugins.RobotGraphiquePlugin {
	private static final int version = 1;
	private boolean actif = true;

	public boolean actif() {
		return this.actif;
	}

	public void setActif(boolean value) {
		this.actif = value;
	}

	public void display(Robot robot, Graphics g) {
		int x = robot.getCaseCourante().getX();
		int y = robot.getCaseCourante().getY();
		int larg = robot.getCaseCourante().getLarg();
		String imgFileName = "robot_" + Integer.toString(robot.getId())
				+ ".png";

		File imageFile = new File(Paths.get("classes", imgFileName)
				.toAbsolutePath().toString());
		try {
			Image img = new ImageIcon(imageFile.toURI().toURL()).getImage();
			BufferedImage resized = resize(img, larg, larg);
			g.drawImage(resized, x * larg - 1, y * larg - 1, null);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}

	BufferedImage resize(Image originalImage, int width, int height) {
		BufferedImage resized = new BufferedImage(width, height,
				BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = resized.createGraphics();
		g.setComposite(AlphaComposite.Src);
		g.drawImage(originalImage, 0, 0, width, height, null);
		g.dispose();
		return resized;
	}

	public String getLibelle() {
		return ("Avec Image");
	}

	public int getVersion() {
		return version;
	}

}

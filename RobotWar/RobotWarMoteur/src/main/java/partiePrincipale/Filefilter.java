package partiePrincipale;

import java.io.File;
import java.io.FilenameFilter;
import java.util.regex.Matcher;

public class Filefilter implements FilenameFilter{

	private String filtre;
	
	/*----------------------CONSTRUCTEUR-------------------------*/
	public Filefilter(String filter){
		filtre = filter;
	}
	
	public Filefilter(Matcher matcher){
		filtre = matcher.pattern().toString();
	}
	
	/*----------------------METHODE-------------------------*/
	@Override
	public boolean accept(File dir, String name) {
		return name.endsWith(filtre);
	}

}

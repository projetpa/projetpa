package partiePrincipale;

import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;

public class Cadrillage extends Affichage {

	private int dimension;
	private int largeur;
	private List<Case> cases;

	/*----------------------CONSTRUCTEUR-------------------------*/
	public Cadrillage(int dimension, int largeur) {
		this.dimension = dimension;
		this.largeur = largeur;
		this.cases = new ArrayList();
		Case.largeur = this.largeur / (dimension);

		for (int i = 0; i < this.dimension; i++) {
			for (int j = 0; j < this.dimension; j++) {
				cases.add(new Case(j, i));
			}
		}
	}

	/*----------------------METHODE-------------------------*/
	@Override
	public void draw(Graphics g) {
		for (Case c : this.cases) {
			c.draw(g);
		}
	}
	
	/*----------------------GETTERS-------------------------*/
	public int getDimension() {
		return dimension;
	}

	public Case getCase(int x, int y) {
		int i = y * 30 + x;

		return this.cases.get(i);
	}

	public Case getCase(Case currentCase, int direction) {
		int x = currentCase.getX();
		int y = currentCase.getY();

		switch (direction) {
		case 0:
			if (y < 29)
				return getCase(x, y + 1);
			break;

		case 1:
			if (x < 29)
				return getCase(x + 1, y);
			break;

		case 2:
			if (y > 0)
				return getCase(x, y - 1);
			break;
		case 3:
			if (x > 0)
				return getCase(x - 1, y);
			break;
		default:
			break;
		}
		return null;
	}

	public int getDistance(Case case1, Case case2) {
		int x1;
		int x2;
		int y1;
		int y2;
		x1 = case1.getX();
		x2 = case2.getX();
		y1 = case1.getY();
		y2 = case2.getY();

		return Math.abs(x1 - x2) + Math.abs(y1 - y2);
	}

	public Case getCaseSiExistante(int x, int y) {
		if ((x >= 0) && (x < 30) && (y >= 0) && (y < 30))
			return MoteurJeu.getSingleton().getCadrillage().getCase(x, y);

		return null;
	}

	public List<Case> getCases() {
		return cases;
	}

	public int getLargeur() {
		return largeur;
	}

	/*----------------------SETTERS-------------------------*/
	public void setDimension(int dimension) {
		this.dimension = dimension;
	}
	
	public void setLargeur(int largeur) {
		this.largeur = largeur;
	}

	public void setCases(List<Case> cases) {
		this.cases = cases;
	}
}

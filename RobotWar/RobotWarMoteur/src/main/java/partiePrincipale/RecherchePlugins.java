package partiePrincipale;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import partiePrincipale.Repository;

public class RecherchePlugins {

	public void recherche(String path, final Repository loader) {
		FileVisitor fileVisitor = new FileVisitor() {

			public FileVisitResult postVisitDirectory(Object arg0,
					IOException arg1) throws IOException {
				return FileVisitResult.CONTINUE;
			}

			public FileVisitResult preVisitDirectory(Object arg0,
					BasicFileAttributes arg1) throws IOException {
				return FileVisitResult.CONTINUE;
			}

			public FileVisitResult visitFile(Object arg0,
					BasicFileAttributes arg1) throws IOException {
				String fileName = arg0.toString();
				if (RecherchePlugins.this.match(fileName))
					loader.getBase().add(arg0.toString());
				return FileVisitResult.CONTINUE;
			}

			public FileVisitResult visitFileFailed(Object arg0, IOException arg1)
					throws IOException {
				return null;
			}

		};

		try {
			Files.walkFileTree(Paths.get(path), fileVisitor);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public boolean match(String s) {
		Pattern p = Pattern.compile(".*jar");
		Matcher m = p.matcher(s);

		return m.matches();
	}

}

package partiePrincipale;

import java.awt.Color;

import partiePrincipale.Case;
import partiePrincipale.IA;
import partiePrincipale.Robot;

public class Joueur {

	private String nom;
	private IA ia;
	private Robot robot;
	private static int robotId = 0;

	/*----------------------CONSTRUCTEUR-------------------------*/
	public Joueur(String nom, IA ia, Case startCase, Color couleur) {
		super();
		this.nom = nom;
		this.ia = ia;
		robotId++;
		this.robot = new Robot(this, robotId, (Case) startCase, couleur);
	}

	/*----------------------METHODE-------------------------*/
	public void jouer() {
		this.ia.jouer(this);
	}
	
	/*----------------------GETTERS-------------------------*/
	public String getNom() {
		return nom;
	}

	public IA getIa() {
		return ia;
	}

	public Robot getRobot() {
		return robot;
	}

	/*----------------------SETTERS-------------------------*/
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public void setRobot(Robot robot) {
		this.robot = robot;
	}
	
	public void setIa(IA ia) {
		this.ia = ia;
	}
}

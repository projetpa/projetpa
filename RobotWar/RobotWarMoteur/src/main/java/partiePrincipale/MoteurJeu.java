package partiePrincipale;

import java.awt.Color;
import java.util.ArrayList;

import partieGraphique.FenetreJeu;
import partieGraphique.FenetrePrincipale;
import plugins.ArmeGraphiquePlugin;
import plugins.AttaquePlugin;
import plugins.DeplacementPlugin;
import plugins.Plugin;
import plugins.RobotGraphiquePlugin;

public class MoteurJeu {

	private static MoteurJeu moteur = new MoteurJeu();

	private ArrayList<Joueur> joueurs;
	private ArrayList<DeplacementPlugin> deplacement;
	private ArrayList<AttaquePlugin> attaque;
	private ArrayList<RobotGraphiquePlugin> robot;
	private ArrayList<ArmeGraphiquePlugin> arme;
	private Cadrillage cadrillage;
	public FenetreJeu fenetreJeu;
	private int currentPlayer;

	/*----------------------CONSTRUCTEUR-------------------------*/
	public MoteurJeu() {
		super();
		this.deplacement = new ArrayList();
		this.attaque = new ArrayList();
		this.robot = new ArrayList();
		this.arme = new ArrayList();
		this.joueurs = new ArrayList();
		this.cadrillage = new Cadrillage(30, 600);
	}

	/*----------------------SINGLETON-------------------------*/
	public static MoteurJeu getSingleton() {
		return moteur;
	}

	/*----------------------MAIN-------------------------*/
	public static void main(String[] args) throws Exception {
		MoteurJeu moteurJeu = new MoteurJeu();
		IA ia1 = new IA(0);
		IA ia2 = new IA(0);
		IA ia3 = new IA(0);
		IA ia4 = new IA(0);

		MoteurJeu.getSingleton().joueurs.add(new Joueur("Robot Bleu", ia1, MoteurJeu
				.getSingleton().cadrillage.getCase(0, 0), Color.BLUE));
		MoteurJeu.getSingleton().joueurs.add(new Joueur("Robot Rouge", ia2,
				MoteurJeu.getSingleton().cadrillage.getCase(0, 29), Color.RED));
		MoteurJeu.getSingleton().joueurs.add(new Joueur("Robot Vert", ia3, MoteurJeu
				.getSingleton().cadrillage.getCase(29, 0), Color.GREEN));
		MoteurJeu.getSingleton().joueurs.add(new Joueur("Robot Jaune", ia4, MoteurJeu
				.getSingleton().cadrillage.getCase(29, 29), Color.YELLOW));

		Arme canon = new Arme(0);

		for (Joueur joueur : MoteurJeu.getSingleton().joueurs) {
			canon.attribuerArme(joueur.getRobot());
		}

		FenetrePrincipale mainFrame = new FenetrePrincipale();
	}

	/*----------------------METHODES-------------------------*/
	public void lancerJeu() {
		ArrayList<Thread> threads = new ArrayList();

		for (Joueur joueur : this.joueurs) {
			threads.add(new Thread(new JouerPartie(joueur)));
		}

		for (Thread thread : threads) {
			thread.start();
		}

	}

	public String[] remplissageAttaque(AttaquePlugin[] plugins) {
		String[] libelles = new String[plugins.length];

		for (int index = 0; index < plugins.length; index++) {

			this.attaque.add(plugins[index]);

			libelles[index] = plugins[index].getLibelle();
		}

		return libelles;
	}

	public String[] remplissageRobot(RobotGraphiquePlugin[] plugins) {
		String[] libelles = new String[plugins.length];

		for (int index = 0; index < plugins.length; index++) {

			this.robot.add(plugins[index]);

			libelles[index] = plugins[index].getLibelle();
		}

		return libelles;
	}

	public String[] remplissageArme(ArmeGraphiquePlugin[] plugins) {
		String[] libelles = new String[plugins.length];

		for (int index = 0; index < plugins.length; index++) {

			this.arme.add(plugins[index]);

			libelles[index] = plugins[index].getLibelle();
		}

		return libelles;
	}

	public String[] remplissageDeplacement(DeplacementPlugin[] plugins) {
		String[] libelles = new String[plugins.length];

		for (int index = 0; index < plugins.length; index++) {

			this.deplacement.add(plugins[index]);

			libelles[index] = plugins[index].getLibelle();
		}

		return libelles;
	}

	public void changer(String libelle, boolean state) {
		for (Plugin plugin : this.deplacement) {
			if (plugin.getLibelle().equals(libelle))
				plugin.setActifAndSave(state);
		}
		for (Plugin plugin : this.attaque) {
			if (plugin.getLibelle().equals(libelle))
				plugin.setActifAndSave(state);
		}
		for (Plugin plugin : this.robot) {
			if (plugin.getLibelle().equals(libelle))
				plugin.setActifAndSave(state);
		}
		for (Plugin plugin : this.arme) {
			if (plugin.getLibelle().equals(libelle))
				plugin.setActifAndSave(state);
		}
	}

	/*----------------------GETTERS-------------------------*/
	public ArrayList<Joueur> getJoueurs() {
		return joueurs;
	}

	public ArrayList<DeplacementPlugin> getDeplacementPlugins() {
		ArrayList<DeplacementPlugin> plugins = new ArrayList();

		for (DeplacementPlugin plugin : this.deplacement) {
			if (plugin.actif())
				plugins.add(plugin);
		}

		return plugins;
	}

	public ArrayList<AttaquePlugin> getAttaquePlugins() {
		ArrayList<AttaquePlugin> plugins = new ArrayList();

		for (AttaquePlugin plugin : this.attaque) {
			if (plugin.actif())
				plugins.add(plugin);
		}

		return plugins;
	}

	public ArrayList<RobotGraphiquePlugin> getRobotGraphiquePlugins() {
		ArrayList<RobotGraphiquePlugin> plugins = new ArrayList();

		for (RobotGraphiquePlugin plugin : this.robot) {
			if (plugin.actif())
				plugins.add(plugin);
		}

		return plugins;
	}

	public ArrayList<ArmeGraphiquePlugin> getArmeGraphiquePlugins() {
		ArrayList<ArmeGraphiquePlugin> plugins = new ArrayList();

		for (ArmeGraphiquePlugin plugin : this.arme) {
			if (plugin.actif())
				plugins.add(plugin);
		}

		return plugins;
	}

	public Cadrillage getCadrillage() {
		return cadrillage;
	}

	public FenetreJeu getFenetreJeu() {
		return fenetreJeu;
	}

	public int getCurrentPlayer() {
		return currentPlayer;
	}

	public Plugin getLibelle(String libelle) {
		for (Plugin plugin : this.deplacement) {
			if (plugin.getLibelle().equals(libelle))
				return plugin;
		}
		for (Plugin plugin : this.attaque) {
			if (plugin.getLibelle().equals(libelle))
				return plugin;
		}
		for (Plugin plugin : this.robot) {
			if (plugin.getLibelle().equals(libelle))
				return plugin;
		}
		for (Plugin plugin : this.arme) {
			if (plugin.getLibelle().equals(libelle))
				return plugin;
		}

		return null;
	}

	/*----------------------SETTERS-------------------------*/
	public void setJoueurs(ArrayList<Joueur> joueurs) {
		this.joueurs = joueurs;
	}

	public void setDeplacementPlugins(
			ArrayList<DeplacementPlugin> deplacementPlugins) {
		this.deplacement = deplacementPlugins;
	}

	public void setAttaquePlugins(ArrayList<AttaquePlugin> attaquePlugins) {
		this.attaque = attaquePlugins;
	}

	public void setRobotGraphiquePlugins(
			ArrayList<RobotGraphiquePlugin> robotGraphiquePlugins) {
		this.robot = robotGraphiquePlugins;
	}

	public void setArmeGraphiquePlugins(
			ArrayList<ArmeGraphiquePlugin> armeGraphiquePlugins) {
		this.arme = armeGraphiquePlugins;
	}

	public void setCadrillage(Cadrillage cadrillage) {
		this.cadrillage = cadrillage;
	}

	public void setFenetreJeu(FenetreJeu fenetreJeu) {
		this.fenetreJeu = fenetreJeu;
	}

	public void setCurrentPlayer(int currentPlayer) {
		this.currentPlayer = currentPlayer;
	}
}
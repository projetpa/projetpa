package partiePrincipale;

import java.util.ArrayList;
import java.util.Random;

import partieGraphique.FenetreJeu;
import partiePrincipale.Case;
import partiePrincipale.Joueur;
import partiePrincipale.Robot;
import plugins.AttaquePlugin;
import plugins.DeplacementPlugin;

public class IA {

	private int niveau;
	private boolean checkEnergieDeplacement;
	private boolean approcher;
	private int minEnergie;

	/*----------------------CONSTRUCTEUR-------------------------*/
	public IA(int niveau) {
		super();
		this.niveau = niveau;
		this.initialize(niveau);
	}

	/*----------------------METHODE-------------------------*/
	private void initialize(int niveau) {

		switch (niveau) {
		case 0:
			this.checkEnergieDeplacement = true;
			this.minEnergie = 6;
			approcher = true;
			break;
		default:
			break;
		}
	}

	public boolean isCheckEnergieDeplacement() {
		return checkEnergieDeplacement;
	}

	public void setCheckEnergieDeplacement(boolean checkEnergieDeplacement) {
		this.checkEnergieDeplacement = checkEnergieDeplacement;
	}

	public boolean estApprocher() {
		return approcher;
	}

	public void setApprocher(boolean approcher) {
		this.approcher = approcher;
	}

	public int getMinEnergie() {
		return minEnergie;
	}

	public void setMinEnergie(int minEnergie) {
		this.minEnergie = minEnergie;
	}

	public int getNiveau() {
		return niveau;
	}

	public void jouer(Joueur joueur) {
		boolean attaque = false;
		attaque = attaquer(joueur);
		if (!attaque) {
			if (joueur.getRobot().getEnergie() > minEnergie){
				seDeplacer(joueur);
			}
		}
	}

	public boolean seDeplacer(Joueur joueur) {
		if (this.approcher)
			return approcher(joueur);
		else
			return deplacement(joueur);
	}

	public boolean deplacement(Joueur joueur) {
		ArrayList<DeplacementPlugin> deplacements = MoteurJeu.getSingleton()
				.getDeplacementPlugins();
		ArrayList<Case> casesAccessibles;

		for (DeplacementPlugin plugin : deplacements) {
			if (plugin.estPossible(joueur, this.checkEnergieDeplacement)) {
				Random rand = new Random();
				casesAccessibles = plugin.casesAccessibles(MoteurJeu
						.getSingleton().getCadrillage(), joueur);

				Case cible = casesAccessibles.get(rand
						.nextInt(casesAccessibles.size()));
				return plugin.faitDeplacement(joueur, cible,
						this.checkEnergieDeplacement);
			}
		}

		return false;
	}

	public boolean approcher(Joueur joueur) {
		ArrayList<DeplacementPlugin> deplacements = MoteurJeu.getSingleton()
				.getDeplacementPlugins();
		ArrayList<Case> casesAccessibles = new ArrayList<Case>();
		ArrayList<Case> casesEnemies = new ArrayList<Case>();
		int distanceMin = 450;
		Case cible = null;

		for (DeplacementPlugin plugin : deplacements) {
			if (plugin.estPossible(joueur, this.checkEnergieDeplacement))
				casesAccessibles.addAll(plugin.casesAccessibles(MoteurJeu
						.getSingleton().getCadrillage(), joueur));
		}

		for (Robot robot : Robot.robots) {
			if (!robot.equals(joueur.getRobot()))
				casesEnemies.add(robot.getCaseCourante());
		}

		for (Case maCase : casesAccessibles) {
			for (Case saCase : casesEnemies) {
				int distance = MoteurJeu.getSingleton().getCadrillage()
						.getDistance(maCase, saCase);
				if (distance < distanceMin) {
					distanceMin = distance;
					cible = maCase;
				}
			}
		}

		if (cible == null)
			return false;

		for (DeplacementPlugin plugin : deplacements) {
			if (plugin.faitDeplacement(joueur, cible, checkEnergieDeplacement))
				return true;
		}

		return false;
	}

	public boolean attaquer(Joueur joueur) {
		ArrayList<AttaquePlugin> attaques = MoteurJeu.getSingleton()
				.getAttaquePlugins();

		for (AttaquePlugin attaque : attaques) {
			Robot robot = attaque.peutAttaquer(joueur.getRobot());

			if (robot != null) {
				attaque.attaque(joueur.getRobot(), robot);
				return true;
			}
		}
		return false;
	}

	public String getLibelle() {
		switch (this.niveau) {
		case 0:
			return "Random";
		default:
			return "Random";
		}
	}

}

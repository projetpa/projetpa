package partiePrincipale;

import java.awt.Color;
import java.awt.Graphics;

public class Case {

	private int x;
	private int y;
	protected static int largeur;
	protected boolean libre;

	/*----------------------CONSTRUCTEUR-------------------------*/
	public Case(int x, int y) {
		super();
		this.x = x;
		this.y = y;
		this.libre = true;
	}

	/*----------------------METHODES-------------------------*/
	public void draw(Graphics g) {
		g.setColor(Color.BLACK);
		g.fillRect(x * largeur - 1, y * largeur - 1, largeur, largeur);
	}
	
	public boolean estLibre() {
		return this.libre;
	}

	/*----------------------GETTERS-------------------------*/
	public Case getCase(int direction) {
		return MoteurJeu.getSingleton().getCadrillage()
				.getCase(this, direction);
	}

	public int getDistance(Case autreCase) {
		return MoteurJeu.getSingleton().getCadrillage()
				.getDistance(this, autreCase);
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
	
	public int getLarg() {
		return largeur;
	}
	/*----------------------SETTERS-------------------------*/
	public void setX(int value) {
		this.x = value;
	}

	public void setY(int value) {
		this.y = value;
	}
}

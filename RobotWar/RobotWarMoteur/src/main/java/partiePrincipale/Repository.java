package partiePrincipale;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import plugins.ArmeGraphiquePlugin;
import plugins.AttaquePlugin;
import plugins.DeplacementPlugin;
import plugins.RobotGraphiquePlugin;
import annotations.PluginAnnotation;

public class Repository {

	private File base;
	private File baseB;
	private List<String> listString = new ArrayList<>();
	private boolean initialized;
	private ArrayList<String> files;
	private ArrayList classArme;
	private ArrayList classRobot;
	private ArrayList classDeplacement;
	private ArrayList classAttaque;

	public Repository(File base) {
		this.base = base;
		this.baseB = base;
		this.files = new ArrayList();
		this.initialized = false;
		this.classArme = new ArrayList();
		this.classRobot = new ArrayList();
		this.classDeplacement = new ArrayList();
		this.classAttaque = new ArrayList();
		try {
			RecherchePlugins lister = new RecherchePlugins();
			lister.recherche(base.getAbsolutePath().toString(), this);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void listerProfondeur() {
		if (this.initialized)
			return;
		Filefilter filtre = new Filefilter(".class");
		File[] listeFile = base.listFiles(filtre);
		if (base.listFiles() != null) {
			for (int i = 0; i < base.listFiles().length; i++) {
				if (base.listFiles()[i].isDirectory()) {
					listeFile = Arrays.copyOf(listeFile, listeFile.length + 1);
					listeFile[listeFile.length - 1] = base.listFiles()[i];
				}
			}
		}
		if (listeFile != null) {
			for (File f : listeFile) {
				if (f.isDirectory()) {
					base = f;
					listerProfondeur();
				} else {
					listString.add(f.toString());
				}
			}
		} else {
			try {
				JarFile zip = new JarFile(base.getAbsolutePath());
				for (Enumeration<JarEntry> entries = zip.entries(); entries
						.hasMoreElements();) {
					JarEntry entry = entries.nextElement();
					if (entry.isDirectory()) {
						continue;
					}
					String name = entry.getName();
					Class tmpClass = null;
					if (name.endsWith(".class")) {
						URL[] classLoaderUrls = new URL[] { new URL("file:///"
								+ base.getAbsolutePath()) };
						URLClassLoader urlClassLoader = new URLClassLoader(
								classLoaderUrls);

						while (entries.hasMoreElements()) {
							if (name.length() > 6
									&& name.substring(name.length() - 6)
											.compareTo(".class") == 0) {
								name = name.substring(0, name.length() - 6);
								name = name.replaceAll("/", ".");
								try {
									tmpClass = Class.forName(name, true,
											urlClassLoader);
									if (tmpClass
											.isAnnotationPresent(PluginAnnotation.class)) {
										Annotation annotation = tmpClass
												.getAnnotation(PluginAnnotation.class);
										PluginAnnotation annotationPlugin = (PluginAnnotation) annotation;
										String pluginType = annotationPlugin
												.type();
										if (pluginType
														.equals("DeplacementPlugin")) {
											this.classDeplacement.add(tmpClass);
										} else if (pluginType
														.equals("ArmeGraphique")) {
											this.classArme.add(tmpClass);
										} else if (pluginType
														.equals("RobotGraphique")) {
											this.classRobot.add(tmpClass);
										} else if (pluginType.equals("Attaque")) {
											this.classAttaque.add(tmpClass);
										}
									}
								} catch (ClassNotFoundException e) {
									e.printStackTrace();
								}

							}
							name = entries.nextElement().toString();
						}

					}
				}
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		this.initialized = true;
	}

	public List<Class<?>> load() {
		listerProfondeur();
		return null;
	}
	
	public AttaquePlugin[] loadAllAttaquePlugins() throws Exception {
		this.listerProfondeur();
		AttaquePlugin[] tmpPlugins = new AttaquePlugin[this.classAttaque.size()];
		for (int index = 0; index < tmpPlugins.length; index++) {
			tmpPlugins[index] = (AttaquePlugin) ((Class) this.classAttaque
					.get(index)).newInstance();
		}
		return tmpPlugins;
	}
	
	public ArrayList getClassAttaque(){
		return classAttaque;
	}

	public ArmeGraphiquePlugin[] loadAllArmeGraphiquePlugins() throws Exception {
		this.listerProfondeur();
		ArmeGraphiquePlugin[] tmpPlugins = new ArmeGraphiquePlugin[this.classArme
				.size()];
		for (int index = 0; index < tmpPlugins.length; index++) {
			tmpPlugins[index] = (ArmeGraphiquePlugin) ((Class) this.classArme
					.get(index)).newInstance();
		}
		return tmpPlugins;
	}

	public RobotGraphiquePlugin[] loadAllRobotGraphiquePlugins()
			throws Exception {
		this.listerProfondeur();
		RobotGraphiquePlugin[] tmpPlugins = new RobotGraphiquePlugin[this.classRobot
				.size()];
		for (int index = 0; index < tmpPlugins.length; index++) {
			tmpPlugins[index] = (RobotGraphiquePlugin) ((Class) this.classRobot
					.get(index)).newInstance();
		}
		return tmpPlugins;
	}

	public DeplacementPlugin[] loadAllDeplacementPlugins() throws Exception {
		this.listerProfondeur();
		DeplacementPlugin[] tmpPlugins = new DeplacementPlugin[this.classDeplacement
				.size()];
		for (int index = 0; index < tmpPlugins.length; index++) {
			tmpPlugins[index] = (DeplacementPlugin) ((Class) this.classDeplacement
					.get(index)).newInstance();
		}
		return tmpPlugins;
	}

	public ArrayList<String> getBase() {
		ArrayList<String> retour = new ArrayList();
		retour.add(base.getName());
		return retour;
	}

}

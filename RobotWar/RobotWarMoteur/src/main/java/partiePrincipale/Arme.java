package partiePrincipale;

public class Arme {
	private int id;
	private String libelle;
	private int degat;
	private int portee;

	/*----------------------CONSTRUCTEUR-------------------------*/
	public Arme(int id) throws Exception {
		if (id == 0) {
			this.id = 0;
			this.libelle = "Pistolet";
			this.degat = 5;
			this.portee = 5;
		} else {
			throw new Exception("Cet id n'existe pas");
		}

	}

	/*----------------------METHODES-------------------------*/
	public void attribuerArme(Robot robot) {
		robot.ajouterArme(this);
	}

	/*----------------------GETTERS-------------------------*/
	public int getDegat() {
		return this.degat;
	}

	public int getPortee() {
		return this.portee;
	}

	public String getLibelle() {
		return this.libelle;
	}

	public int getId() {
		return id;
	}

	/*----------------------SETTERS-------------------------*/
	public void setId(int id) {
		this.id = id;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public void setDegat(int degat) {
		this.degat = degat;
	}

	public void setPortee(int portee) {
		this.portee = portee;
	}
}

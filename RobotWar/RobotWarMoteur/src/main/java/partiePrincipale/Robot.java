package partiePrincipale;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;

import partieGraphique.FenetreInformation;
import plugins.ArmeGraphiquePlugin;
import plugins.RobotGraphiquePlugin;

public class Robot extends Affichage {
	protected static List<Robot> robots = new ArrayList();
	private int energie;
	private int id;
	private boolean actif;
	private List<Arme> armes;
	private Color couleur;
	private Case caseCourante;
	private FenetreInformation fenetreInfo;
	private int direction = 0;
	private Robot cible;
	private Joueur joueur;

	/*----------------------CONSTRUCTEUR-------------------------*/
	public Robot(Joueur joueur, int id, Case caseCourante, Color couleur) {
		super();
		this.energie = 800;
		this.id = id;
		this.couleur = couleur;
		this.caseCourante = caseCourante;
		this.actif = true;
		this.armes = new ArrayList();
		this.joueur = joueur;
		robots.add(this);
		this.fenetreInfo = new FenetreInformation(this);
	}

	/*----------------------METHODE-------------------------*/
	public boolean estActif() {
		return actif;
	}

	public void deplacer(Case uneCase) {
		int resx = uneCase.getX() - this.getCaseCourante().getX();
		int resy = uneCase.getY() - this.getCaseCourante().getY();
		if (caseCourante != null) {
			caseCourante.libre = true;
		}
		caseCourante = uneCase;
		caseCourante.libre = false;

		if (resx < 0) {
			direction = 3;
		}
		if (resx > 0) {
			direction = 1;
		}
		if (resy < 0) {
			direction = 0;
		}
		if (resy > 0) {
			direction = 2;
		}

		if (MoteurJeu.getSingleton().getFenetreJeu() != null) {
			MoteurJeu.getSingleton().getFenetreJeu().repaint();
		}
	}

	public void ajouterArme(Arme arme) {
		armes.add(arme);
		fenetreInfo.setArmes(this.armes);
	}

	public void supprimerArme(Arme arme) {
		armes.remove(arme);
	}

	@Override
	public void draw(Graphics g) {
		int derniereVersion = -1;
		RobotGraphiquePlugin dernierRobotPlugin = null;

		for (RobotGraphiquePlugin plugin : MoteurJeu.getSingleton()
				.getRobotGraphiquePlugins()) {
			if (plugin.getVersion() > derniereVersion) {
				derniereVersion = plugin.getVersion();
				dernierRobotPlugin = plugin;
			}
		}
		if (dernierRobotPlugin != null) {
			dernierRobotPlugin.display(this, g);
		}

		for (ArmeGraphiquePlugin plugin : MoteurJeu.getSingleton()
				.getArmeGraphiquePlugins()) {
			plugin.display(MoteurJeu.getSingleton().getCadrillage(), this, g);
		}
	}

	public void perteEnergie(int valeur) {
		this.setEnergie(energie - valeur);

		if (this.energie < 1)
			this.setActif(false);
	}

	public List<Robot> getEnnemis() {
		ArrayList<Robot> ennemis = new ArrayList();

		for (Joueur joueurs : MoteurJeu.getSingleton().getJoueurs()) {
			Robot robot = joueurs.getRobot();

			if ((!robot.equals(this)) && (robot.estActif()))
				ennemis.add(robot);
		}

		return ennemis;
	}

	public boolean possedeArme(int id) {
		for (Arme arme : armes) {
			if (arme.getId() == id)
				return true;
		}
		return false;
	}
	
	/*----------------------GETTERS-------------------------*/
	public Robot getCible() {
		return cible;
	}

	public static List<Robot> getRobots() {
		return robots;
	}

	public List<Arme> getArmes() {
		return this.armes;
	}

	public Case getCaseCourante() {
		return caseCourante;
	}

	public FenetreInformation getFenetreInfo() {
		return fenetreInfo;
	}

	public Joueur getJoueur() {
		return joueur;
	}

	public int getEnergie() {
		return energie;
	}

	public int getDirection() {
		return direction;
	}

	public Color getCouleur() {
		return couleur;
	}

	public int getId() {
		return id;
	}

	/*----------------------SETTERS-------------------------*/
	public void setDirection(int direction) {
		this.direction = direction;
	}

	public void setJoueur(Joueur joueur) {
		this.joueur = joueur;
	}

	public void setArmes(List<Arme> armes) {
		this.armes = armes;
	}

	public static void setRobots(List<Robot> robots) {
		Robot.robots = robots;
	}

	public void setCaseCourante(Case caseCourante) {
		this.caseCourante = caseCourante;
	}

	public void setFenetreInfo(FenetreInformation fenetreInfo) {
		this.fenetreInfo = fenetreInfo;
	}

	public void setCible(Robot cible) {
		this.cible = cible;
	}

	public void setActif(boolean actif) {
		this.actif = actif;
		if (!(this.actif)) {
			robots.remove(this);
			MoteurJeu.getSingleton().getFenetreJeu().removeDrawable(this);
		}
	}

	public void setEnergie(int energie) {
		this.energie = energie;
		fenetreInfo.setEnergie(energie);
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setCouleur(Color nouvelleCouleur) {
		this.couleur = nouvelleCouleur;
	}
}

package partiePrincipale;

public class JouerPartie implements Runnable {
	private final static int WAINTING_TIME = 300;
	Joueur joueur;

	/*----------------------CONSTRUCTEUR-------------------------*/
	public JouerPartie(Joueur joueur) {
		this.joueur = joueur;
	}
	
	/*----------------------METHODE-------------------------*/
	public synchronized void run() {
		while (Robot.robots.size() > 1 && joueur.getRobot().estActif()) {
			try {
				Thread.sleep(WAINTING_TIME);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			joueur.jouer();
		}
	}

}

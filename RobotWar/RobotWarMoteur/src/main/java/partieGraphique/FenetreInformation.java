package partieGraphique;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JPanel;

import partiePrincipale.Arme;
import partiePrincipale.Robot;

@SuppressWarnings("serial")
public class FenetreInformation extends JPanel {
	JLabel energie;
	JLabel nom;
	JLabel id;
	JLabel ia;
	JLabel armes;

	/*----------------------CONSTRUCTEUR-------------------------*/
	public FenetreInformation(Robot robot) {
		Dimension dim = new Dimension(200, 150);
		GridLayout myLayout = new GridLayout(7, 1);
		this.setLayout(myLayout);

		this.energie = new JLabel();
		this.nom = new JLabel();
		this.id = new JLabel();
		this.ia = new JLabel();
		this.armes = new JLabel();

		this.add(id);
		this.add(nom);
		this.add(energie);
		this.add(ia);
		this.add(armes);

		this.setId(robot.getId());
		this.setNom(robot.getJoueur().getNom());
		this.setEnergie(robot.getEnergie());
		this.setIA(robot.getJoueur().getIa().getLibelle());

		this.setPreferredSize(dim);
	}
	
	/*----------------------METHODE-------------------------*/
	@Override
	public void paint(Graphics g) {
		super.paint(g);
	}
	
	/*----------------------SETTERS-------------------------*/
	public void setEnergie(int energie) {
		this.energie.setText("Energie : " + energie);
	}

	public void setId(int id) {
		this.id.setText("Id du robot : " + id);
	}

	public void setNom(String nom) {
		this.nom.setText("Nom : " + nom);
	}

	public void setIA(String ia) {
		this.ia.setText(ia);
	}

	public void setArmes(List<Arme> armes) {
		String s = "Armes : ";

		for (Arme a : armes) {
			s += a.getLibelle();
			s += " ";
		}
		this.armes.setText(s);
	}
}

package partieGraphique;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

import partiePrincipale.MoteurJeu;
import partiePrincipale.Repository;
import partiePrincipale.Robot;

@SuppressWarnings("serial")
public class FenetrePrincipale extends JFrame implements ActionListener {

	private JMenuBar menuBar;

	private JMenuItem ajouter;
	private JMenuItem charger;
	private JMenuItem demarrer;

	private JList<String> armeList;
	private JList<String> robotList;
	private JList<String> deplacementList;
	private JList<String> attaqueList;

	private ArrayList<String> files;
	private List<Robot> robot;
	private Repository pluginsLoader;

	/*----------------------CONSTRUCTEUR-------------------------*/
	public FenetrePrincipale() {
		this.initialisation();
		this.robot = Robot.getRobots();
		this.setVisible(true);

	}

	/*----------------------METHODE-------------------------*/
	public void addPlugin(String path) {
		this.files.add(path);

	}

	public void initialisation() {
		this.menuBar = new JMenuBar();
		this.ajouter = new JMenuItem();
		this.charger = new JMenuItem();
		this.demarrer = new JMenuItem();

		this.armeList = new JList();
		this.robotList = new JList();
		this.deplacementList = new JList();
		this.attaqueList = new JList();

		this.files = new ArrayList();

		this.robotList.setEnabled(false);
		this.armeList.setEnabled(false);
		this.deplacementList.setEnabled(false);
		this.attaqueList.setEnabled(false);

		this.menuBar.add(this.demarrer);
		this.menuBar.add(this.ajouter);
		this.menuBar.add(this.charger);

		this.ajouter.setText("Ajouter un plugin");
		this.charger.setText("Charger plugin");
		this.demarrer.setText("Lancer partie");

		this.setSize(800, 600);
		this.setJMenuBar(this.menuBar);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.demarrer.addActionListener(this);
		this.ajouter.addActionListener(this);
		this.charger.addActionListener(this);

		this.setTitle("Robot War");
		this.setLocationRelativeTo(null);
	}

	public void ouvrirPlugins() {
		JFileChooser f = new JFileChooser();

		if (f.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
			this.addPlugin(f.getSelectedFile().getAbsolutePath());
		}
	}

	public void chargerPlugins() {
		File f = new File(this.files.get(0));
		this.pluginsLoader = new Repository(f);

		try {
			this.remplirListePlugins();
		} catch (Exception e) {
			javax.swing.JOptionPane.showMessageDialog(null, e.getMessage());
		}
	}

	public void demarrerJeu()

	{
		
		FenetreJeu fenetreJeu = new FenetreJeu();
		this.setTitle("Robot War");
		this.setLocationRelativeTo(null);
		JPanel panel = new JPanel(new BorderLayout());
		JPanel InfoContainer = new JPanel(new GridLayout());

		InfoContainer.setLayout(new GridLayout(Robot.getRobots().size(), 1));
		for (int i = 0; i < robot.size(); i++) {
			InfoContainer.add(Robot.getRobots().get(i).getFenetreInfo());
		}
    
		panel.add(new FenetrePlugins(), BorderLayout.WEST);
		panel.add(fenetreJeu, BorderLayout.CENTER);
		panel.add(InfoContainer, BorderLayout.SOUTH);
		InfoContainer.setLayout(new GridLayout(1,4));
		
		this.setContentPane(panel);
		panel.setVisible(true);
		this.pack();
		MoteurJeu.getSingleton().lancerJeu();

	}

	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource() == this.ajouter) {
			this.ouvrirPlugins();
		} else if (arg0.getSource() == this.charger) {
			this.chargerPlugins();
		} else if (arg0.getSource() == this.demarrer) {
			this.demarrerJeu();
		}
	}

	private void remplirListePlugins() throws Exception {
		this.attaqueList.setListData(MoteurJeu.getSingleton()
				.remplissageAttaque(this.pluginsLoader.loadAllAttaquePlugins()));
		this.robotList.setListData(MoteurJeu.getSingleton()
				.remplissageRobot(
						this.pluginsLoader.loadAllRobotGraphiquePlugins()));
		this.armeList.setListData(MoteurJeu.getSingleton()
				.remplissageArme(
						this.pluginsLoader.loadAllArmeGraphiquePlugins()));
		this.deplacementList.setListData(MoteurJeu.getSingleton()
				.remplissageDeplacement(
						this.pluginsLoader.loadAllDeplacementPlugins()));
	}

	public static void main(String[] args) {
		FenetrePrincipale test = new FenetrePrincipale();
	}
}
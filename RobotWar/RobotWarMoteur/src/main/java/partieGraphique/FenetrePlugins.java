package partieGraphique;

import java.awt.GridLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import javax.swing.JCheckBox;
import javax.swing.JPanel;

import partiePrincipale.MoteurJeu;
import plugins.Plugin;

@SuppressWarnings("serial")
public class FenetrePlugins extends JPanel {
	ArrayList<String> plugins;

	/*----------------------CONSTRUCTEUR-------------------------*/
	public FenetrePlugins() {
		this.plugins = new ArrayList<String>();
		this.trouverPlugins();
		this.initialisation();
	}

	/*----------------------METHODE-------------------------*/
	private void trouverPlugins() {
		for (Plugin plugin : MoteurJeu.getSingleton().getRobotGraphiquePlugins()) {
			plugins.add(plugin.getLibelle());
		}
		for (Plugin plugin : MoteurJeu.getSingleton().getArmeGraphiquePlugins()) {
			plugins.add(plugin.getLibelle());
		}
		for (Plugin plugin : MoteurJeu.getSingleton().getAttaquePlugins()) {
			plugins.add(plugin.getLibelle());
		}
		for (Plugin plugin : MoteurJeu.getSingleton().getDeplacementPlugins()) {
			plugins.add(plugin.getLibelle());
		}
	}

	private void initialisation() {
		this.setLayout(new GridLayout(plugins.size(), 1));
		for (String s : plugins) {
			boolean actif = true;
			Plugin plugin = MoteurJeu.getSingleton().getLibelle(s);

			if (plugin != null) {
				plugin.load();
				actif = plugin.actif();
			}

			final JCheckBox check = new JCheckBox(s, actif);

			check.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					String libelle = check.getText();
					boolean state = check.isSelected();
					MoteurJeu.getSingleton().changer(
							libelle, state);
				}
			});

			this.add(check);
		}
	}
}
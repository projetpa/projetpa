package partieGraphique;

import java.awt.Dimension;
import java.awt.Graphics;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JPanel;

import partiePrincipale.Affichage;
import partiePrincipale.Joueur;
import partiePrincipale.MoteurJeu;

@SuppressWarnings("serial")
public class FenetreJeu extends JPanel {
	private List<Affichage> drawables = new LinkedList<Affichage>();

	/*----------------------CONSTRUCTEUR-------------------------*/
	public FenetreJeu() {
		super();
		MoteurJeu.getSingleton().fenetreJeu = this;
		
		Dimension dim = new Dimension(600, 600);
		this.setPreferredSize(dim);

		this.addDrawable(MoteurJeu.getSingleton().getCadrillage());

		for (Joueur joueur : MoteurJeu.getSingleton().getJoueurs()) {
			this.addDrawable(joueur.getRobot());
		}
	}

	/*----------------------METHODE-------------------------*/
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		try {
			for (Iterator<Affichage> iter = drawables.iterator(); iter
					.hasNext();) {
				Affichage d = (Affichage) iter.next();
				if (d != null)
					d.draw(g);
			}
		} catch (Exception e) {
		}
	}

	public void addDrawable(Affichage d) {
		drawables.add(d);
		repaint();
	}

	public void removeDrawable(Affichage d) {
		drawables.remove(d);
		repaint();
	}

	public void clear() {
		drawables.clear();
		repaint();
	}

}

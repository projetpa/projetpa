package plugins;

import partiePrincipale.Robot;

public interface AttaquePlugin extends Plugin {
	public Robot peutAttaquer(Robot robot);

	public int valeurAttaque();

	public void attaque(Robot source, Robot cible);

	public int getCout();
}

package plugins;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public interface Plugin {

	public String getLibelle();

	public boolean actif();

	public void setActif(boolean value);

	public default void setActifAndSave(boolean value) {
		setActif(value);
		save();
	}

	public default void save() {
		File f = new File(this.getLibelle() + ".txt");
		String str = "";
		try {
			FileWriter fw = new FileWriter(f);

			if (this.actif())
				str = "enabled";
			else
				str = "disabled";

			fw.write(str);
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public default void load() {
		File f = new File(this.getLibelle() + ".txt");
		String str = "";
		try {
			FileReader fr = new FileReader(f);
			char[] cbuf = new char[100];
			fr.read(cbuf, 0, 100);
			for (int i = 0; i < cbuf.length; i++) {
				str += cbuf[i];
			}

			if (str.contains("disabled"))
				this.setActif(false);
			else
				this.setActif(true);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

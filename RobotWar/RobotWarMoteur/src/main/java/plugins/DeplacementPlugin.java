package plugins;

import java.util.ArrayList;

import partiePrincipale.Cadrillage;
import partiePrincipale.Case;
import partiePrincipale.Joueur;
import partiePrincipale.Robot;

public interface DeplacementPlugin extends Plugin {
	public default boolean faitDeplacement(Joueur joueur, Case cible,
			boolean checkEnergie) {
		Robot robot = joueur.getRobot();

		if (!estPossible(joueur, checkEnergie))
			return false;

		robot.deplacer(cible);
		robot.perteEnergie(coutDeplacement());
		return true;
	}

	public ArrayList<Case> casesAccessibles(Cadrillage cadrillage,
			Joueur joueur);

	public boolean estPossible(Joueur joueur, boolean checkEnergie);

	public int coutDeplacement();

	public int iaLevelMin();
}
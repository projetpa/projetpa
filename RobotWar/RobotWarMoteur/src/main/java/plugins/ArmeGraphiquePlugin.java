package plugins;

import java.awt.Graphics;

import partiePrincipale.Cadrillage;
import partiePrincipale.Robot;

public interface ArmeGraphiquePlugin extends Plugin {

	void display(Cadrillage cadrillage, Robot robot, Graphics g);
}

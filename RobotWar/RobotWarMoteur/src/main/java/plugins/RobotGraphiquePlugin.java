package plugins;

import java.awt.Graphics;

import partiePrincipale.Robot;

public interface RobotGraphiquePlugin extends Plugin {

	public void display(Robot robot, Graphics g);

	int getVersion();

}